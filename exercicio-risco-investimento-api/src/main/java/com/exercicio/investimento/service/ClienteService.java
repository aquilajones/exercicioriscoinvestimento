package com.exercicio.investimento.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exercicio.investimento.model.Cliente;
import com.exercicio.investimento.model.Risco;
import com.exercicio.investimento.repository.Clientes;
@Service
public class ClienteService {
	@Autowired
	private Clientes clientes;
	
	public Cliente adicionar(Cliente cliente) {
		for (Risco risco : Risco.values()) {
			if (risco.name().equalsIgnoreCase(cliente.getRisco())) {
				cliente.setAliquotaJuros(new BigDecimal(risco.valorRisco));
			}
		}
		return clientes.save(cliente);
	}

}
