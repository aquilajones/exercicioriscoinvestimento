package com.exercicio.investimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExercicioRiscoInvestimentoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExercicioRiscoInvestimentoApiApplication.class, args);
	}

}
