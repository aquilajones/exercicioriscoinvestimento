package com.exercicio.investimento.resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exercicio.investimento.model.Cliente;
import com.exercicio.investimento.model.Risco;
import com.exercicio.investimento.repository.Clientes;
import com.exercicio.investimento.service.ClienteService;

@CrossOrigin("*")
@RestController
@RequestMapping("/clientes")
public class ClientesResource {
	
	@Autowired
	private Clientes clientes;
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	public List<Cliente> listar(){
		return clientes.findAll();
	}
	
	@PostMapping
	public Cliente adicionar(@RequestBody Cliente cliente) {
		return clienteService.adicionar(cliente);
	}
	
	
	@GetMapping(path="/riscos")
	public List<RiscoVO> listarRiscos(){
		
		List<RiscoVO> list = new ArrayList<RiscoVO>();
		 Arrays.asList( Risco.values()).forEach(r -> list.add(new RiscoVO(r.toString())));
		
		return list;
	}
	
}
