package com.exercicio.investimento.resource;

public class RiscoVO {
	private String descricao;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public RiscoVO(String descricao) {

		this.descricao = descricao;
	}

}
