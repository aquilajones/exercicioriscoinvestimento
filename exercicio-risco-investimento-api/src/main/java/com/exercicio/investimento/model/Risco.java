package com.exercicio.investimento.model;

public enum Risco {
	A(0), B(10), C(20);

	public int valorRisco;

	Risco(int valor) {
		valorRisco = valor;
	}

}
