package com.exercicio.investimento.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.exercicio.investimento.model.Cliente;

public interface Clientes extends JpaRepository<Cliente, Long> {

}
