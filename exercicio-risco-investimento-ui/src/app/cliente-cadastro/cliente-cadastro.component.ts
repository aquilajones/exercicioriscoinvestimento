import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ClientesService } from '../clientes/clientes.service';
import { FormGroup } from '@angular/forms';


@Component({
  selector: 'app-cliente-cadastro',
  templateUrl: './cliente-cadastro.component.html',
  styleUrls: ['./cliente-cadastro.component.css']
})
export class ClienteCadastroComponent implements OnInit {
  
  cliente:any;
  riscos : Array<any>;
  risco : any;
  @Output() clienteSalvo = new EventEmitter();
  constructor(private clientesSerive :ClientesService) { }

  ngOnInit() {
    this.clientesSerive.listarRisco().subscribe(response => this.riscos = response);
   
    this.initAtributos();
  }
  initAtributos(){
    this.cliente ={};
  }
  submit(frm: FormGroup){
    this.cliente.risco =  this.risco.descricao;
    this.clientesSerive.adicionar(this.cliente).subscribe(response => {
        
         this.initAtributos();
         this.clienteSalvo.emit(response);
       });

  }

}
