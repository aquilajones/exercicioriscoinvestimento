import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { TableModule} from "primeng/table";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClientesListagemComponent } from './clientes-listagem/clientes-listagem.component';
import {HttpClientModule} from "@angular/common/http";
import { ClienteCadastroComponent } from './cliente-cadastro/cliente-cadastro.component';
import {  DropdownModule} from "primeng/dropdown";
import {  InputTextModule} from "primeng/inputtext";
import {  CurrencyMaskModule} from "ng2-currency-mask";
import {  FormsModule} from "@angular/forms";
@NgModule({
  declarations: [
    AppComponent,
    ClientesListagemComponent,
    ClienteCadastroComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    TableModule,
    AppRoutingModule,
    HttpClientModule,
    DropdownModule,
    InputTextModule,
    CurrencyMaskModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
