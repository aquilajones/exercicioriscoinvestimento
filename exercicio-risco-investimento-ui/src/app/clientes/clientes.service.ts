import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  private api = 'http://localhost:8080';

  constructor(private http: HttpClient ) { }

  listar() : Observable<any>{
    return this.http.get<any>( 'http://localhost:8080/clientes' );
  }


  listarRisco() : Observable<any>{
    return this.http.get<any>( 'http://localhost:8080/clientes/riscos' );
  }

  adicionar(cliente: any) : Observable<any>{
    return this.http.post('http://localhost:8080/clientes',cliente);
  }

}
