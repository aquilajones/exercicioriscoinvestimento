import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../clientes/clientes.service';

import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-clientes-listagem',
  templateUrl: './clientes-listagem.component.html',
  styleUrls: ['./clientes-listagem.component.css']
})
export class ClientesListagemComponent implements OnInit {

  clientes: Array<any>;
  constructor(private clienteService : ClientesService) { }

  ngOnInit() {
    this.listar();
  }

  listar(){
    this.clienteService.listar().subscribe(response => this.clientes = response);
  }


}
